package com.mfs.client.emq.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

public class EMQClientLive {

	static String baseUrl = "http://localhost:8080/MFSEMQWeb/";
	// static String baseUrl = "http://emqprod.us-east-2.elasticbeanstalk.com/";

	String getAuthResult() throws Exception {

		String params = "";

		Map<String, Object> jsonResponse1 = new HashMap<String, Object>();

		String response = authService();

		jsonResponse1 = jsonToMapObject1(response);

		if (jsonResponse1 != null && response.contains("result")) {

			params = (String) jsonResponse1.get("result");

			return params;

		} else {

			params = "Fail";
			return params;
		}
	}

	String retrieveQuoteService(String source, String destination) throws Exception {
		String retrieveQuoteServiceUrl = baseUrl + "quotes/" + source + "/" + destination;
		return getConnectionjsonResponseGet(retrieveQuoteServiceUrl);
	}

	String getlistRelationshipsService() throws Exception {
		String listRelationshipsServiceUrl = baseUrl + "listRelationships";
		return getConnectionjsonResponseGet(listRelationshipsServiceUrl);
	}

	String getlistCurrenciesService() throws Exception {
		String listCurrenciesServiceUrl = baseUrl + "listCurrencies";
		return getConnectionjsonResponseGet(listCurrenciesServiceUrl);
	}

	String getlistDestinationsService(String country) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listDestinationssServiceUrl = baseUrl + "countries/" + country + "/destinations";
		return getConnectionjsonResponseGet(listDestinationssServiceUrl);
	}

	String getlistSourcesService(String country) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listSourcesServiceUrl = baseUrl + "countries/" + country + "/sources";
		return getConnectionjsonResponseGet(listSourcesServiceUrl);
	}

	String getlistAllDocumentTypesService(String country) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listAllDocumentTypesServiceUrl = baseUrl + "countries/" + country + "/documentTypes";
		return getConnectionjsonResponseGet(listAllDocumentTypesServiceUrl);
	}

	String getlistSegmentSourcesOfFundsService(String country, String segment) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listSegmentSourcesOfFundsServiceUrl = baseUrl + "countries/" + country + "/segmentSourcesOfFunds/"
				+ segment;
		return getConnectionjsonResponseGet(listSegmentSourcesOfFundsServiceUrl);
	}

	String listSegmentRemittancePurposesService(String country, String segment) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listSegmentRemittancePurposesServiceUrl = baseUrl + "countries/" + country + "/remittancePurposes/"
				+ segment;
		return getConnectionjsonResponseGet(listSegmentRemittancePurposesServiceUrl);
	}

	String getListSourcesOfFundsService(String country) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listSegmentRemittancePurposesServiceUrl = baseUrl + "countries/" + country + "/sourcesOfFunds";
		return getConnectionjsonResponseGet(listSegmentRemittancePurposesServiceUrl);
	}

	String listRemittancePurposesService(String country) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listAllStatesServiceUrl = baseUrl + "countries/" + country + "/remittancePurposes";
		return getConnectionjsonResponseGet(listAllStatesServiceUrl);
	}

	String listAllCitiesService(String country, String state) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listAllBranchesServiceUrl = baseUrl + "countries/" + country + "/states/" + state + "/cities";
		return getConnectionjsonResponseGet(listAllBranchesServiceUrl);
	}

	String listAllStatesService(String country) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listAllStatesServiceUrl = baseUrl + "countries/" + country + "/states";
		return getConnectionjsonResponseGet(listAllStatesServiceUrl);
	}

	String listAllBranchesService(String country, String bank) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listAllBranchesServiceUrl = baseUrl + "countries/" + country + "/banks/" + bank + "/branches";
		return getConnectionjsonResponseGet(listAllBranchesServiceUrl);
	}

	String listallBanksService(String country) throws Exception {
		country = new Run().get3DigitCountryCode(country);
		String listallBanksServiceUrl = baseUrl + "countries/" + country + "/banks";
		return getConnectionjsonResponseGet(listallBanksServiceUrl);
	}

	String listAllCountriesService() throws Exception {
		String listAllCountriesServiceUrl = baseUrl + "listAllCountries";
		return getConnectionjsonResponseGet(listAllCountriesServiceUrl);
	}

	String listAllActiveCorridorsService() throws Exception {
		String listAllCorridorsServiceUrl = baseUrl + "listAllActiveCorridors";
		return getConnectionjsonResponseGet(listAllCorridorsServiceUrl);
	}

	String authService() throws Exception {
		String authServiceUrl = baseUrl + "login";
		return getConnectionjsonResponseGet(authServiceUrl);
	}

	String cancelTransferService(String reference) throws Exception {
		String cancleTransferServiceUrl = baseUrl + "cancelTransfer/" + reference + "/cancel";
		return getConnectionjsonResponseGet(cancleTransferServiceUrl);
	}

	String confirmTransferService(String reference) throws Exception {
		String confirmTransferServiceUrl = baseUrl + "confirmTransfer/" + reference + "/confirm";
		return getConnectionjsonResponseGet(confirmTransferServiceUrl);
	}

	String getTransferService(String reference) throws Exception {
		String getTransferServiceUrl = baseUrl + "getTransfer/" + reference;
		return getConnectionjsonResponseGet(getTransferServiceUrl);
	}

	String getBalanceService() throws Exception {
		String balanceServiceUrl = baseUrl + "balances";
		return getConnectionjsonResponseGet(balanceServiceUrl);
	}

	String createTransferIndiaC2C(String reference, String destinationAmountCurrency, String destinationAmountUnits,
			String sourceType, String sourceCountry, String sourceSegment, String sourceLegalNameFirst,
			String sourceLegalNameLast, String sourceDateOfBirth, String sourceNationality, String sourceIdType,
			String sourceIdCountry, String sourceIdNumber, String sourceAddressCity, String sourceAddressLine,
			String destinationtype, String destinationCountry, String destinationLegalNameFirst,
			String destinationLegalNameLast, String destinationMobileNumber, String destinationBank,
			String destinationBranch, String destinationAccountNumber, String destinationAddressLine,
			String destinationAddressCity, String sourceOfFunds, String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferIndiaC2C/" + reference;
		JsonObject indiaC2CServiceRequest = new JsonObject();

		JsonObject destinatinAmount = new JsonObject();
		destinatinAmount.addProperty("currency", destinationAmountCurrency);
		destinatinAmount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("segment", sourceSegment);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_line", sourceAddressLine);

		JsonObject destination = new JsonObject();
		destination.addProperty("type", destinationtype);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("legal_name_first", destinationLegalNameFirst);
		destination.addProperty("legal_name_last", destinationLegalNameLast);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("bank", destinationBank);
		destination.addProperty("branch", destinationBranch);

		destination.addProperty("account_number", destinationAccountNumber);
		destination.addProperty("address_line", destinationAddressLine);
		destination.addProperty("address_city", destinationAddressCity);

		JsonObject compliance = new JsonObject();
		compliance.addProperty("source_of_funds", sourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		indiaC2CServiceRequest.add("destination_amount", destinatinAmount);
		indiaC2CServiceRequest.add("source", source);
		indiaC2CServiceRequest.add("destination", destination);
		indiaC2CServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, indiaC2CServiceRequest.toString());
	}

	String createTransferIndiaC2CWallet(String reference, String destinationAmountCurrency,
			String destinationAmountUnits, String sourceType, String sourceCountry, String sourceSegment,
			String sourceLegalNameFirst, String sourceLegalNameLast, String sourceDateOfBirth, String sourceNationality,
			String sourceIdType, String sourceIdCountry, String sourceIdNumber, String sourceAddressCity,
			String sourceAddressLine, String destinationtype, String destinationCountry,
			String destinationLegalNameFirst, String destinationLegalNameLast, String destinationMobileNumber,
			String ewalletId, String destinationAddressLine, String destinationAddressCity, String sourceOfFunds,
			String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferIndiaC2CWallet/" + reference;
		JsonObject indiaC2CServiceRequest = new JsonObject();

		indiaC2CServiceRequest.addProperty("reference", reference);
		JsonObject destinatinAmount = new JsonObject();
		destinatinAmount.addProperty("currency", destinationAmountCurrency);
		destinatinAmount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("segment", sourceSegment);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_line", sourceAddressLine);

		JsonObject destination = new JsonObject();
		destination.addProperty("type", destinationtype);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("legal_name_first", destinationLegalNameFirst);
		destination.addProperty("legal_name_last", destinationLegalNameLast);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("ewallet_id", ewalletId);
		destination.addProperty("address_line", destinationAddressLine);
		destination.addProperty("address_city", destinationAddressCity);

		JsonObject compliance = new JsonObject();
		compliance.addProperty("source_of_funds", sourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		indiaC2CServiceRequest.add("destination_amount", destinatinAmount);
		indiaC2CServiceRequest.add("source", source);
		indiaC2CServiceRequest.add("destination", destination);
		indiaC2CServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, indiaC2CServiceRequest.toString());
	}

	String createTransferIndiaB2B(String reference, String destinationAmountCurrency, String destinationAmountUnits,
			String sourceType, String sourceCountry, String sourceSenderId, String sourceSegment,
			String sourceCompanyName, String sourceCompanyTradingName, String sourceNativeCompanyName,
			String sourceCompanyRegistrationNumber, String sourcecompanyRegistrationCountry, String sourceCompanyType,
			String sourceLegalNameFirst, String sourceLegalNameLast, String sourceMobileNumber, String sourceEmail,
			String sourceAddressLine, String sourceAddressCity, String sourceAddressState, String sourceAddressCountry,
			String sourceAddressZip, String destinationType, String destinationCountry, String destinationSegment,
			String destinationCompanyName, String destinationCompanyRegistrationNumber, String destinationMobileNumber,
			String destinationBank, String destinationBranch, String destinationAccountNumber,
			String destinationAddressLine, String destinationAddressCity, String complianceSourceOfFunds,
			String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferIndiaB2B/" + reference;
		JsonObject indiaB2BServiceRequest = new JsonObject();
		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);
		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("sender_id", sourceSenderId);
		source.addProperty("segment", sourceSegment);
		source.addProperty("company_name", sourceCompanyName);
		source.addProperty("company_trading_name", sourceCompanyTradingName);
		source.addProperty("native_company_name", sourceNativeCompanyName);
		source.addProperty("company_registration_number", sourceCompanyRegistrationNumber);
		source.addProperty("company_registration_country", sourcecompanyRegistrationCountry);
		source.addProperty("company_type", sourceCompanyType);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("mobile_number", sourceMobileNumber);
		source.addProperty("email", sourceEmail);
		source.addProperty("address_line", sourceAddressLine);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_state", sourceAddressState);
		source.addProperty("address_country", sourceAddressCountry);
		source.addProperty("address_zip", sourceAddressZip);
		JsonObject destination = new JsonObject();
		destination.addProperty("type", destinationType);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("segment", destinationSegment);
		destination.addProperty("company_name", destinationCompanyName);
		destination.addProperty("company_registration_number", destinationCompanyRegistrationNumber);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("bank", destinationBank);
		destination.addProperty("branch", destinationBranch);
		destination.addProperty("account_number", destinationAccountNumber);
		destination.addProperty("address_line", destinationAddressLine);
		destination.addProperty("address_city", destinationAddressCity);
		JsonObject compliance = new JsonObject();
		compliance.addProperty("source_of_funds", complianceSourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);
		indiaB2BServiceRequest.add("destination_amount", destination_amount);
		indiaB2BServiceRequest.add("source", source);
		indiaB2BServiceRequest.add("destination", destination);
		indiaB2BServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, indiaB2BServiceRequest.toString());
	}

	String createTransferChinaC2C(String reference, String destinationAmountCurrency, String destinationAmountUnits,
			String sourceType, String sourceCountry, String sourceSegment, String sourceLegalNameFirst,
			String sourceLegalNameLast, String sourceDateOfBirth, String sourceNationality, String sourceIdType,
			String sourceIdCountry, String sourceIdNumber, String sourceAddressCity, String sourceAddressLine,
			String sourcAddressCountry, String destinationtype, String destinationCountry,
			String destinationLegalNameFirst, String destinationLegalNameLast, String destinationMobileNumber,
			String destinationAccountNumber, String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferChinaC2C/" + reference;
		JsonObject ChinaC2CServiceRequest = new JsonObject();

		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("segment", sourceSegment);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_line", sourceAddressLine);
		source.addProperty("address_country", sourcAddressCountry);

		JsonObject destination = new JsonObject();
		destination.addProperty("type", destinationtype);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("legal_name_first", destinationLegalNameFirst);
		destination.addProperty("legal_name_last", destinationLegalNameLast);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("account_number", destinationAccountNumber);

		JsonObject compliance = new JsonObject();
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		ChinaC2CServiceRequest.add("destination_amount", destination_amount);
		ChinaC2CServiceRequest.add("source", source);
		ChinaC2CServiceRequest.add("destination", destination);
		ChinaC2CServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, ChinaC2CServiceRequest.toString());
	}

	String createTransferJapanB2B(String reference, String destinationAmountCurrency, String destinationAmountUnits,
			String sourceType, String sourceCountry, String sourceSenderId, String sourceSegment,
			String sourceCompanyName, String sourceCompanyTradingName, String sourceNativeCompanyName,
			String sourceCompanyRegistrationNumber, String sourcecompanyRegistrationCountry, String sourceCompanyType,
			String sourceLegalNameFirst, String sourceLegalNameLast, String sourceMobileNumber, String sourceEmail,
			String sourceAddressLine, String sourceAddressCity, String sourceAddressState, String sourceAddressZip,
			String sourceAddressCountry, String destinationType, String destinationCountry, String destinationSegment,
			String destinationCompanyName, String destinationNativeCompanyname, String destinationMobileNumber,
			String destinationBank, String destinationBranch, String destinationAccountNumber,
			String destinationAddressLine, String destinationAddressCity, String destinationAddressZip,
			String destinationCompanyRegistrationCountry, String complianceSourceOfFunds,
			String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferJapanB2B/" + reference;
		JsonObject japanB2BServiceRequest = new JsonObject();
		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);
		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("sender_id", sourceSenderId);
		source.addProperty("segment", sourceSegment);
		source.addProperty("company_name", sourceCompanyName);
		source.addProperty("company_trading_name", sourceCompanyTradingName);
		source.addProperty("native_company_name", sourceNativeCompanyName);
		source.addProperty("company_registration_number", sourceCompanyRegistrationNumber);
		source.addProperty("company_registration_country", sourcecompanyRegistrationCountry);
		source.addProperty("company_type", sourceCompanyType);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("mobile_number", sourceMobileNumber);
		source.addProperty("email", sourceEmail);
		source.addProperty("address_line", sourceAddressLine);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_state", sourceAddressState);
		source.addProperty("address_zip", sourceAddressZip);
		source.addProperty("address_country", sourceAddressCountry);
		JsonObject destination = new JsonObject();
		destination.addProperty("type", destinationType);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("segment", destinationSegment);
		destination.addProperty("company_name", destinationCompanyName);
		destination.addProperty("native_company_name", destinationNativeCompanyname);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("bank", destinationBank);
		destination.addProperty("branch", destinationBranch);
		destination.addProperty("account_number", destinationAccountNumber);
		destination.addProperty("address_line", destinationAddressLine);
		destination.addProperty("address_city", destinationAddressCity);
		destination.addProperty("address_zip", destinationAddressZip);
		destination.addProperty("company_registration_country", destinationCompanyRegistrationCountry);
		JsonObject compliance = new JsonObject();
		compliance.addProperty("source_of_funds", complianceSourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);
		japanB2BServiceRequest.add("destination_amount", destination_amount);
		japanB2BServiceRequest.add("source", source);
		japanB2BServiceRequest.add("destination", destination);
		japanB2BServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, japanB2BServiceRequest.toString());
	}

	String createTransferIndonesiaB2B(String reference, String destinationAmountCurrency, String destinationAmountUnits,
			String sourceType, String sourceCountry, String sourceSenderId, String sourceSegment,
			String sourceCompanyName, String sourceCompanyTradingName, String sourceNativeCompanyName,
			String sourceCompanyRegistrationNumber, String sourcecompanyRegistrationCountry, String sourceCompanyType,
			String sourceLegalNameFirst, String sourceLegalNameLast, String sourceMobileNumber, String sourceEmail,
			String sourceAddressLine, String sourceAddressCity, String sourceAddressState, String sourceAddressCountry,
			String sourceAddressZip, String destinationType, String destinationCountry, String destinationSegment,
			String destinationCompanyName, String destinationMobileNumber, String destinationCompanyRegistrationNumber,
			String destinationCompanyRegistrationCountry, String destinationBank, String destinationAccountNumber,
			String destinationAddressLine, String destinationAddressCity, String destinationAddressState,
			String complianceSourceOfFunds, String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferIndonesiaB2B/" + reference;
		JsonObject japanB2BServiceRequest = new JsonObject();
		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);
		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("sender_id", sourceSenderId);
		source.addProperty("segment", sourceSegment);
		source.addProperty("company_name", sourceCompanyName);
		source.addProperty("company_trading_name", sourceCompanyTradingName);
		source.addProperty("native_company_name", sourceNativeCompanyName);
		source.addProperty("company_registration_number", sourceCompanyRegistrationNumber);
		source.addProperty("company_registration_country", sourcecompanyRegistrationCountry);
		source.addProperty("company_type", sourceCompanyType);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("mobile_number", sourceMobileNumber);
		source.addProperty("email", sourceEmail);
		source.addProperty("address_line", sourceAddressLine);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_state", sourceAddressState);
		source.addProperty("address_country", sourceAddressCountry);
		source.addProperty("address_zip", sourceAddressZip);
		JsonObject destination = new JsonObject();
		destination.addProperty("type", destinationType);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("segment", destinationSegment);
		destination.addProperty("company_name", destinationCompanyName);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("company_registration_number", destinationCompanyRegistrationNumber);
		destination.addProperty("company_registration_country", destinationCompanyRegistrationCountry);
		destination.addProperty("bank", destinationBank);
		destination.addProperty("account_number", destinationAccountNumber);
		destination.addProperty("address_line", destinationAddressLine);
		destination.addProperty("address_city", destinationAddressCity);
		destination.addProperty("address_state", destinationAddressState);
		JsonObject compliance = new JsonObject();
		compliance.addProperty("source_of_funds", complianceSourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);
		japanB2BServiceRequest.add("destination_amount", destination_amount);
		japanB2BServiceRequest.add("source", source);
		japanB2BServiceRequest.add("destination", destination);
		japanB2BServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, japanB2BServiceRequest.toString());
	}

	String createTransferIndonesiaC2CBankAccount(String reference, String destinationAmountCurrency,
			String destinationAmountUnits, String sourceType, String sourceCountry, String sourceSegment,
			String sourceLegalNameFirst, String sourceMobileNumber, String sourceDateOfBirth, String sourceNationality,
			String sourceIdType, String sourceIdCountry, String sourceIdNumber, String sourceAddressCity,
			String sourceAddressLine, String sourcAddressCountry, String destinationType, String destinationCountry,
			String destinationLegalNameFirst, String destinationMobileNumber, String destinationIdNumber,
			String destinationBank, String destinationAccountNumber, String destinationAddressLine,
			String destinationAddressCity, String destinationAddressState, String complianceRemittancePurpose)
			throws Exception {

		String emqServiceUrl = baseUrl + "createIndonesiaC2CBankAccount/" + reference;
		JsonObject ChinaC2CServiceRequest = new JsonObject();

		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("segment", sourceSegment);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("mobile_number", sourceMobileNumber);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_line", sourceAddressLine);
		source.addProperty("address_country", sourcAddressCountry);
		JsonObject destination = new JsonObject();

		destination.addProperty("type", destinationType);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("legal_name_first", destinationLegalNameFirst);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("id_number", destinationIdNumber);
		destination.addProperty("bank", destinationBank);
		destination.addProperty("account_number", destinationAccountNumber);
		destination.addProperty("address_city", destinationAddressCity);
		destination.addProperty("address_line", destinationAddressLine);
		destination.addProperty("address_state", destinationAddressState);
		JsonObject compliance = new JsonObject();
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		ChinaC2CServiceRequest.add("destination_amount", destination_amount);
		ChinaC2CServiceRequest.add("source", source);
		ChinaC2CServiceRequest.add("destination", destination);
		ChinaC2CServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, ChinaC2CServiceRequest.toString());
	}

	String createTransferIndonesiaC2CCashPickUp(String reference, String destinationAmountCurrency,
			String destinationAmountUnits, String sourceType, String sourceCountry, String sourceSegment,
			String sourceLegalNameFirst, String sourceMobileNumber, String sourceDateOfBirth, String sourceNationality,
			String sourceIdType, String sourceIdCountry, String sourceIdNumber, String sourceAddressCity,
			String sourceAddressLine, String sourcAddressCountry, String destinationType, String destinationPartner,
			String destinationCountry, String destinationLegalNameFirst, String destinationMobileNumber,
			String destinationIdNumber, String destinationAddressLine, String destinationAddressCity,
			String destinationAddressState, String scomplianceSourceOfFunds, String complianceRemittancePurpose)
			throws Exception {

		String emqServiceUrl = baseUrl + "createTransferIndonesiaC2CCashPickup/" + reference;
		JsonObject ChinaC2CServiceRequest = new JsonObject();

		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("segment", sourceSegment);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("mobile_number", sourceMobileNumber);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_line", sourceAddressLine);
		source.addProperty("address_country", sourcAddressCountry);
		JsonObject destination = new JsonObject();

		destination.addProperty("type", destinationType);
		destination.addProperty("partner", destinationPartner);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("legal_name_first", destinationLegalNameFirst);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("id_number", destinationIdNumber);
		destination.addProperty("address_city", destinationAddressCity);
		destination.addProperty("address_line", destinationAddressLine);
		destination.addProperty("address_state", destinationAddressState);
		JsonObject compliance = new JsonObject();
		destination.addProperty("source_of_funds", scomplianceSourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		ChinaC2CServiceRequest.add("destination_amount", destination_amount);
		ChinaC2CServiceRequest.add("source", source);
		ChinaC2CServiceRequest.add("destination", destination);
		ChinaC2CServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, ChinaC2CServiceRequest.toString());
	}

	String createTransferJapanC2c(String reference, String destinationAmountCurrency, String destinationAmountUnits,
			String sourceType, String sourceCountry, String sourceSegment, String sourceLegalNameFirst,
			String sourceLegalNameLast, String sourceDateOfBirth, String sourceNationality, String sourceIdType,
			String sourceIdCountry, String sourceIdNumber, String sourceAddressCity, String sourceAddressLine,
			String sourcAddresszip, String sourcAddressCountry, String destinationType, String destinationCountry,
			String destinationSegment, String destinationLegalNameFirst, String destinationLegalNameLast,
			String destinationNativeLegalNameFirst, String destinationNativeLegalNameLast,
			String destinationMobileNumber, String destinationBank, String destinationBranch,
			String destinationAccountNumber, String destinationAddressLine, String destinationAddressCity,
			String destinationAddressZip, String complianceSourceOfFunds, String complianceRemittancePurpose)
			throws Exception {

		String emqServiceUrl = baseUrl + "createTransferJapanC2C/" + reference;
		JsonObject ChinaC2CServiceRequest = new JsonObject();

		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("segment", sourceSegment);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_line", sourceAddressLine);
		source.addProperty("address_zip", sourcAddresszip);
		source.addProperty("address_country", sourcAddressCountry);
		JsonObject destination = new JsonObject();

		destination.addProperty("type", destinationType);
		destination.addProperty("sourcAddresszip", destinationSegment);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("legal_name_first", destinationLegalNameFirst);
		destination.addProperty("legal_name_last", destinationLegalNameLast);
		destination.addProperty("native_legal_name_first", destinationNativeLegalNameFirst);
		destination.addProperty("native_legal_name_last", destinationNativeLegalNameLast);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("bank", destinationBank);
		destination.addProperty("branch", destinationBranch);
		destination.addProperty("account_number", destinationAccountNumber);
		destination.addProperty("address_city", destinationAddressCity);
		destination.addProperty("address_line", destinationAddressLine);
		destination.addProperty("address_zip", destinationAddressZip);
		JsonObject compliance = new JsonObject();
		destination.addProperty("source_of_funds", complianceSourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		ChinaC2CServiceRequest.add("destination_amount", destination_amount);
		ChinaC2CServiceRequest.add("source", source);
		ChinaC2CServiceRequest.add("destination", destination);
		ChinaC2CServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, ChinaC2CServiceRequest.toString());
	}

	String[] getBranchCode(String country, String bank) throws Exception {

		String[] branchCode = new String[2];
		String response = listAllBranchesService(country, bank);

		Map<String, Object> jsonResponse1 = new HashMap<String, Object>();
		jsonResponse1 = jsonToMapObject1(response);

		if (jsonResponse1 != null && response.contains("code")) {
			branchCode[0] = "01";
			List<Object> list = (List<Object>) jsonResponse1.get("branchesDto");

			if (!(list.isEmpty())) {
				String firstList = list.get(0) + "";
				int s1 = firstList.indexOf("code=");
				int s2 = firstList.indexOf(", name");
				String sub = firstList.substring(s1 + 5, s2);
				branchCode[1] = sub;
			}

		} else {
			branchCode[0] = "02";
			branchCode[1] = "Fail";
		}
		return branchCode;
	}

	String createTransferC2CBankTransfer(String countryCode, String transferType, String reference, String sourceMsisdn,
			String destinationAmountCurrency, String destinationAmountUnits, String sourceCountry,
			String sourceLegalNameFirst, String sourceLegalNameLast, String sourceDateOfBirth, String sourceIdType,
			String sourceIdCountry, String sourceIdNumber, String destinationCountry, String destinationLegalNameFirst,
			String destinationLegalNameLast, String destinationMobileNumber, String destinationBank,
			String destinationAccountNumber) throws Exception {

		String response = "";

		String authResponse = getAuthResult();

		try {

			if (authResponse.equalsIgnoreCase("ok")) {

				sourceIdType = getIdTypes(sourceIdType);

				String branchCode[] = new String[3];

				if (destinationCountry.equalsIgnoreCase("IN") || destinationCountry.equalsIgnoreCase("JP")) {

					branchCode = getBranchCode(destinationCountry, destinationBank);

					if (countryCode.equalsIgnoreCase("IN") && transferType.equalsIgnoreCase("BANK")) {

						if (branchCode[0].equalsIgnoreCase("01"))

							response = createTransferIndiaC2C(reference, destinationAmountCurrency,
									destinationAmountUnits, "partner", sourceCountry, "individual",
									sourceLegalNameFirst, sourceLegalNameLast, sourceDateOfBirth, sourceIdCountry,
									sourceIdType, sourceIdCountry, sourceIdNumber, "sourceaddcity", "addressLine",
									"bank_account", destinationCountry, destinationLegalNameFirst,
									destinationLegalNameLast, destinationMobileNumber, destinationBank, branchCode[1],
									destinationAccountNumber, "destAddLine", "destAddCity", "01", "001-01");

						System.out.println("response==> " + response);

						return response;

					} else if (countryCode.equalsIgnoreCase("JP") && transferType.equalsIgnoreCase("BANK")) {

						if (branchCode[0].equalsIgnoreCase("01"))

							response = createTransferJapanC2c(reference, destinationAmountCurrency,
									destinationAmountUnits, "partner", sourceCountry, "individual",
									sourceLegalNameFirst, sourceLegalNameLast, sourceDateOfBirth, sourceCountry,
									sourceIdType, sourceIdCountry, sourceIdNumber, "addCity", "addLine", "44444",
									sourceIdCountry, "bank_account", destinationCountry, "individual",
									destinationLegalNameFirst, destinationLegalNameLast, destinationLegalNameFirst,
									destinationLegalNameLast, destinationMobileNumber, destinationBank, branchCode[1],
									destinationAccountNumber, "addLine", "addCity", "667877", "01", "001-01");

						System.out.println("response==> " + response);

						return response;
					}

				} else if (countryCode.equalsIgnoreCase("ID") && transferType.equalsIgnoreCase("BANK")) {

					response = createTransferIndonesiaC2CBankAccount(reference, destinationAmountCurrency,
							destinationAmountUnits, "partner", sourceCountry, "individual", sourceLegalNameFirst,
							sourceMsisdn, sourceDateOfBirth, sourceIdCountry, sourceIdType, sourceIdCountry,
							sourceIdNumber, "sourceAddCity", "AddLine1", sourceCountry, "bank_account",
							destinationCountry, destinationLegalNameFirst, destinationMobileNumber, "", destinationBank,
							destinationAccountNumber, "addLine", "addCoty", "01", "001-01");

					System.out.println("response==> " + response);

					return response;

				} else if (countryCode.equalsIgnoreCase("CN") && transferType.equalsIgnoreCase("BANK")) {

					response = createTransferChinaC2C(reference, destinationAmountCurrency, destinationAmountUnits,
							"partner", sourceCountry, "individual", sourceLegalNameFirst, sourceLegalNameLast,
							sourceDateOfBirth, sourceIdCountry, sourceIdType, sourceIdCountry, sourceIdNumber,
							"sourceaddcity", "sourceAddLine1", sourceCountry, "bank_account", destinationCountry,
							destinationLegalNameFirst, destinationLegalNameLast, destinationMobileNumber,
							destinationAccountNumber, "001-01");

					System.out.println("response==> " + response);

					return response;

				} else {

					response = "{\"message\":\"Fail\"}";
				}
				response = "{\"message\":\"Fail\"}";
			}

		} catch (Exception e) {

			response = "{\"message\":\"Fail\"}";
		}
		return response;

	}

	String createTransferC2CTransferWallet(String countryCode, String transferType, String reference,
			String destinationAmountCurrency, String destinationAmountUnits, String sourceCountry,
			String sourceLegalNameFirst, String sourceLegalNameLast, String sourceDateOfBirth, String sourceIdType,
			String sourceIdCountry, String sourceIdNumber, String destinationCountry, String destinationLegalNameFirst,
			String destinationLegalNameLast, String destinationMobileNumber, String ewalletId) throws Exception {

		String response = "";

		String authResponse = getAuthResult();

		try {

			if (authResponse.equalsIgnoreCase("ok")) {

				sourceIdType = getIdTypes(sourceIdType);

				if (destinationCountry.equalsIgnoreCase("IN")) {

					response = createTransferIndiaC2CWallet(reference, destinationAmountCurrency,
							destinationAmountUnits, "partner", sourceCountry, "individual", sourceLegalNameFirst,
							sourceLegalNameLast, sourceDateOfBirth, sourceIdCountry, sourceIdType, sourceIdCountry,
							sourceIdNumber, "sourceaddcity", "addressLine", "ewallet", destinationCountry,
							destinationLegalNameFirst, destinationLegalNameLast, destinationMobileNumber, ewalletId,
							"destAddLine", "destAddCity", "01", "001-01");

					System.out.println("response==> " + response);

					return response;

				} else {

					response = "{\"message\":\"Fail\"}";
				}
			}

		} catch (Exception e) {

			response = "{\"message\":\"Fail\"}";
		}
		return response;

	}

	String createTransferB2BTransfer(String countryCode, String transferType, String reference,
			String destinationAmountCurrency, String destinationAmountUnits, String sourceCountry,
			String sourceSenderId, String sourceCompanyName, String sourceCompanyTradingName,
			String sourceNativeCompanyName, String sourceCompanyRegistrationNumber,
			String sourcecompanyRegistrationCountry, String sourceCompanyType, String sourceLegalNameFirst,
			String sourceLegalNameLast, String sourceMobileNumber, String sourceEmail, String sourceAddressState,
			String sourceAddressCountry, String sourceAddressZip, String destinationCountry,
			String destinationCompanyName, String destinationCompanyRegistrationNumber, String destinationMobileNumber,
			String destinationBank, String destinationBranch, String destinationAccountNumber) throws Exception {

		String response = "";

		// sender = getIdTypes(sourceIdType);
		String branchCode[] = new String[3];
		if (destinationCountry.equalsIgnoreCase("IN") || destinationCountry.equalsIgnoreCase("JP")) {
			branchCode = getBranchCode(destinationCountry, destinationBank);
		}

		try {

			if (countryCode.equalsIgnoreCase("IN") && transferType.equalsIgnoreCase("BANK")) {

				if (branchCode[0].equalsIgnoreCase("01"))
					response = createTransferIndiaB2B(reference, destinationAmountCurrency, destinationAmountUnits,
							"partner", sourceCountry, sourceSenderId, "business", sourceCompanyName,
							sourceCompanyTradingName, sourceNativeCompanyName, sourceCompanyRegistrationNumber,
							sourcecompanyRegistrationCountry, sourceCompanyType, sourceLegalNameFirst,
							sourceLegalNameLast, sourceMobileNumber, sourceEmail, "AddLineSource", "addCitySource",
							sourceAddressState, sourceAddressCountry, sourceAddressZip, "bank_account",
							destinationCountry, "business", destinationCompanyName,
							destinationCompanyRegistrationNumber, destinationMobileNumber, destinationBank,
							destinationBranch, destinationAccountNumber, "destAddLine", "destAddCity", "01", "008-01");

				return response;

			}

		} catch (Exception e) {

		}
		return response;
	}

	String createTransferVietnamC2C(String urlReference,String destinationAmountCurrency,
			String destinationAmountUnits, String sourceType, String sourceCountry,String sourceSenderId, String sourceSegment,
			String sourceLegalNameFirst, String sourceLegalNameLast, String sourceDateOfBirth, String sourceNationality,
			String sourceIdType, String sourceIdCountry, String sourceIdNumber, String sourceAddressCity,
			String destinationType, String destinationPartner, String destinationCountry,
			String destinationLegalNameFirst, String destinationLegalNameLast, String destinationMobileNumber,
			String destinationIdType, String destinationAddressLine, String destinationAddressCity,
			String destinationBank, String destinationAccountNumber, String complianceSourceOfFunds,
			String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferVietnamC2C/" + urlReference;
		JsonObject createTransferVietnamC2CRequest = new JsonObject();

		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("sender_id", sourceSenderId);
		source.addProperty("segment", sourceSegment);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		JsonObject destination = new JsonObject();

		if (destinationType.equals("cash_pickup")) {
			destination.addProperty("type", destinationType);
			destination.addProperty("partner", destinationPartner);
			destination.addProperty("country", destinationCountry);
			destination.addProperty("legal_name_first", destinationLegalNameFirst);
			destination.addProperty("legal_name_last", destinationLegalNameLast);
			destination.addProperty("mobile_number", destinationMobileNumber);
			destination.addProperty("id_type", destinationIdType);

			JsonObject compliance = new JsonObject();
			destination.addProperty("source_of_funds", complianceSourceOfFunds);
			compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

			//createTransferVietnamC2CRequest.addProperty("reference", reference1);
			createTransferVietnamC2CRequest.add("destination_amount", destination_amount);
			createTransferVietnamC2CRequest.add("source", source);
			createTransferVietnamC2CRequest.add("destination", destination);
			createTransferVietnamC2CRequest.add("compliance", compliance);

		} else if (destinationType.equals("cash_delivery")) {

			destination.addProperty("type", destinationType);
			destination.addProperty("partner", destinationPartner);
			destination.addProperty("country", destinationCountry);
			destination.addProperty("legal_name_first", destinationLegalNameFirst);
			destination.addProperty("legal_name_last", destinationLegalNameLast);
			destination.addProperty("mobile_number", destinationMobileNumber);
			destination.addProperty("id_type", destinationIdType);
			destination.addProperty("address_line", destinationAddressLine);
			destination.addProperty("address_city", destinationAddressCity);

			JsonObject compliance = new JsonObject();
			destination.addProperty("source_of_funds", complianceSourceOfFunds);
			compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		//	createTransferVietnamC2CRequest.addProperty("reference", reference1);
			createTransferVietnamC2CRequest.add("destination_amount", destination_amount);
			createTransferVietnamC2CRequest.add("source", source);
			createTransferVietnamC2CRequest.add("destination", destination);
			createTransferVietnamC2CRequest.add("compliance", compliance);

		} else if (destinationType.equals("bank_account")) {

			destination.addProperty("type", destinationType);
			destination.addProperty("country", destinationCountry);
			destination.addProperty("legal_name_first", destinationLegalNameFirst);
			destination.addProperty("legal_name_last", destinationLegalNameLast);
			destination.addProperty("mobile_number", destinationMobileNumber);
			destination.addProperty("bank", destinationBank);
			destination.addProperty("account_number", destinationAccountNumber);

			JsonObject compliance = new JsonObject();
			destination.addProperty("source_of_funds", complianceSourceOfFunds);
			compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		//	createTransferVietnamC2CRequest.addProperty("reference", reference1);
			createTransferVietnamC2CRequest.add("destination_amount", destination_amount);
			createTransferVietnamC2CRequest.add("source", source);
			createTransferVietnamC2CRequest.add("destination", destination);
			createTransferVietnamC2CRequest.add("compliance", compliance);

		}
		return getConnectionjsonResponse(emqServiceUrl, createTransferVietnamC2CRequest.toString());
	}

	String createTransferPhilippinesC2CCashPickUp(String reference, String destinationAmountCurrency,
			String destinationAmountUnits, String sourceType, String sourceCountry, String sourceSegment,
			String sourceSenderId, String sourceLegalNameFirst, String sourceLegalNameLast, String sourceDateOfBirth,
			String sourceNationality, String sourceIdType, String sourceIdCountry, String sourceIdNumber,
			String sourceAddressCity, String sourcAddressCountry, String sourceAddressLine, String destinationType,
			String destinationPartner, String destinationCountry, String destinationLegalNameFirst,
			String destinationLegalNameLast, String destinationMobileNumber, String destinationAddressLine,
			String scomplianceSourceOfFunds, String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferPhilippinesC2CCashPickUp/" + reference;
		JsonObject philippinesC2CCashPickupServiceRequest = new JsonObject();

		JsonObject destination_amount = new JsonObject();
		destination_amount.addProperty("currency", destinationAmountCurrency);
		destination_amount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("segment", sourceSegment);
		source.addProperty("sender_id", sourceSenderId);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_country", sourcAddressCountry);
		source.addProperty("address_line", sourceAddressLine);
		JsonObject destination = new JsonObject();

		destination.addProperty("type", destinationType);
		destination.addProperty("partner", destinationPartner);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("legal_name_first", destinationLegalNameFirst);
		destination.addProperty("legal_name_last", destinationLegalNameLast);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("address_line", destinationAddressLine);

		JsonObject compliance = new JsonObject();
		destination.addProperty("source_of_funds", scomplianceSourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		philippinesC2CCashPickupServiceRequest.add("destination_amount", destination_amount);
		philippinesC2CCashPickupServiceRequest.add("source", source);
		philippinesC2CCashPickupServiceRequest.add("destination", destination);
		philippinesC2CCashPickupServiceRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, philippinesC2CCashPickupServiceRequest.toString());
	}

	String createTransferPhilippinesC2CBankAccount(String reference, String destinationAmountCurrency,
			String destinationAmountUnits, String sourceType, String sourceCountry, String sourceSegment,
			String sourceLegalNameFirst, String sourceLegalNameLast, String sourceDateOfBirth, String sourceNationality,
			String sourceIdType, String sourceIdCountry, String sourceIdNumber, String sourceAddressCity,
			String sourceAddressCountry, String sourceAddressLine, String destinationtype, String destinationCountry,
			String destinationLegalNameFirst, String destinationLegalNameLast, String destinationMobileNumber,
			String destinationBank, String destinationAccountNumber, String destinationAddressLine,
			String sourceOfFunds, String complianceRemittancePurpose) throws Exception {

		String emqServiceUrl = baseUrl + "createTransferPhilippinesC2CBankAccount/" + reference;
		JsonObject philippinesC2CBankAccountRequest = new JsonObject();

		JsonObject destinatinAmount = new JsonObject();
		destinatinAmount.addProperty("currency", destinationAmountCurrency);
		destinatinAmount.addProperty("units", destinationAmountUnits);

		JsonObject source = new JsonObject();
		source.addProperty("type", sourceType);
		source.addProperty("country", sourceCountry);
		source.addProperty("segment", sourceSegment);
		source.addProperty("legal_name_first", sourceLegalNameFirst);
		source.addProperty("legal_name_last", sourceLegalNameLast);
		source.addProperty("date_of_birth", sourceDateOfBirth);
		source.addProperty("nationality", sourceNationality);
		source.addProperty("id_type", sourceIdType);
		source.addProperty("id_country", sourceIdCountry);
		source.addProperty("id_number", sourceIdNumber);
		source.addProperty("address_city", sourceAddressCity);
		source.addProperty("address_country", sourceAddressCountry);
		source.addProperty("address_line", sourceAddressLine);

		JsonObject destination = new JsonObject();
		destination.addProperty("type", destinationtype);
		destination.addProperty("country", destinationCountry);
		destination.addProperty("legal_name_first", destinationLegalNameFirst);
		destination.addProperty("legal_name_last", destinationLegalNameLast);
		destination.addProperty("mobile_number", destinationMobileNumber);
		destination.addProperty("bank", destinationBank);
		destination.addProperty("account_number", destinationAccountNumber);
		destination.addProperty("address_line", destinationAddressLine);

		JsonObject compliance = new JsonObject();
		compliance.addProperty("source_of_funds", sourceOfFunds);
		compliance.addProperty("remittance_purpose", complianceRemittancePurpose);

		philippinesC2CBankAccountRequest.add("destination_amount", destinatinAmount);
		philippinesC2CBankAccountRequest.add("source", source);
		philippinesC2CBankAccountRequest.add("destination", destination);
		philippinesC2CBankAccountRequest.add("compliance", compliance);
		return getConnectionjsonResponse(emqServiceUrl, philippinesC2CBankAccountRequest.toString());
	}

	String getConnectionjsonResponse(String serviceUrl, String request) throws Exception {

		HttpURLConnection connection = null;

		URL url = new URL(serviceUrl);

		connection = (HttpURLConnection) url.openConnection();

		connection.setRequestMethod("POST");
		System.out.println("==> URL:" + serviceUrl);
		System.out.println("==> Request:" + request);
		connection.setRequestProperty("Content-type", "application/json");
		connection.setRequestProperty("Accept", "application/json");
		connection.setUseCaches(false);
		connection.setDoOutput(true);
		connection.setConnectTimeout(20000);

		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		wr.writeBytes(request);
		wr.close();

		InputStream is = connection.getInputStream();
		InputStreamReader isr = new InputStreamReader(is, Charset.forName("UTF-8"));
		BufferedReader rd = new BufferedReader(isr);
		StringBuilder jsonResponse = new StringBuilder();
		String line;

		while ((line = rd.readLine()) != null) {
			jsonResponse.append(line);
			jsonResponse.append('\r');
			// System.out.println("==> Response:"+jsonResponse.toString());
		}

		rd.close();

		if (connection != null) {
			connection.disconnect();
		}

		return jsonResponse.toString();
	}

	String getConnectionjsonResponseGet(String serviceUrl) throws Exception {

		HttpURLConnection connection = null;

		URL url = new URL(serviceUrl);
		connection = (HttpURLConnection) url.openConnection();

		System.out.println("==> url:" + serviceUrl);

		connection.setRequestMethod("GET");

		connection.setUseCaches(false);
		connection.setDoOutput(true);
		connection.setConnectTimeout(20000);

		InputStream is = connection.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader rd = new BufferedReader(isr);
		StringBuilder jsonResponse = new StringBuilder();
		String line;

		while ((line = rd.readLine()) != null) {
			jsonResponse.append(line);
			jsonResponse.append('\r');

			// System.out.println("==> Response:"+jsonResponse.toString());
		}
		rd.close();

		if (connection != null) {
			connection.disconnect();
		}

		return jsonResponse.toString();
	}

	Map<String, String> jsonToMapObject(String jsonString) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Map<String, String> map = new HashMap<String, String>();

		map = mapper.readValue(jsonString, new TypeReference<Map<String, String>>() {
		});

		return map;
	}

	Map<String, Object> jsonToMapObject1(String jsonString) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Map<String, Object> myObjects = mapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {
		});

		myObjects = mapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {
		});

		return myObjects;
	}

	String getIdTypes(String idType) {

		String idName = "";

		if (idType.equalsIgnoreCase("PP")) {

			idName = "passport";

			return idName;

		} else if (idType.equalsIgnoreCase("ID")) {

			idName = "national";

			return idName;
		}

		return idName;
	}

}