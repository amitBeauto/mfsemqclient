package com.mfs.client.emq.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		EMQClientLive eMQClient = new EMQClientLive();

		String response = null;

		String[] jsonResponseParams = new String[3];

		String params = "";

		Map<String, Object> jsonResponse1 = new HashMap<String, Object>();

		Map<String, String> jsonResponse = new HashMap<String, String>();

		int choice = Integer.parseInt(args[0]);

		// int choice = 4;
		try {
			switch (choice) {

			case 1:
				response = eMQClient.authService();
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("result")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.get("result");

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 2:
				response = eMQClient.retrieveQuoteService(args[1], args[2]);

				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("input")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}

				break;

			case 3:
				response = eMQClient.createTransferC2CBankTransfer(args[1], args[2], args[3], args[4], args[5], args[6],
						args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16],
						args[17], args[18], args[19]);

				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hsC2C = (HashMap<String, String>) jsonResponse1.get("info");
				String codeIndiaC2c = hsC2C.get("code");
				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = codeIndiaC2c;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}

				break;

			case 4:
				// response = eMQClient.createTransferB2BTransfer("IN", "BANK", "MFSTEST-005",
				// "INR", "10", "US", "1213243", "Whichshop Pty Ltd", "Whichshop Pty Ltd",
				// "Whichshop Pty Ltd", "1234545", "JP", "LLC", "Some", "one", "+81123456562",
				// "someone@whichshop.co.jp", "OSAKA", "US", "542-0077", "IN", "Example Co",
				// "AFZPK7190K", "+917989458005", "PYTM", "123456", "917259534454");
				response = eMQClient.createTransferB2BTransfer(args[1], args[2], args[3], args[4], args[5], args[6],
						args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16],
						args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24], args[25],
						args[26], args[27]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hsIndiaB2B = (HashMap<String, String>) jsonResponse1.get("info");
				String codeIndiaB2B = hsIndiaB2B.get("code");
				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = codeIndiaB2B;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 5:
				response = eMQClient.getTransferService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hs2 = (HashMap<String, String>) jsonResponse1.get("info");
				String code1 = hs2.get("code");
				if (jsonResponse != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = code1;
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 6:
				response = eMQClient.confirmTransferService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hs3 = (HashMap<String, String>) jsonResponse1.get("info");
				String infoState = hs3.get("state");

				if (jsonResponse1 != null && response.contains("created")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = infoState + "," + jsonResponse1.get("state");

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 7:
				response = eMQClient.listallBanksService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 8:
				response = eMQClient.listAllBranchesService(args[1], args[2]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					List<Object> list = (List<Object>) jsonResponse1.get("branchesDto");
					System.out.println(list.get(0).toString());
					if (!(list.isEmpty())) {
						String firstList = list.get(0) + "";
						int s1 = firstList.indexOf("code=");
						int s2 = firstList.indexOf(", name");
						String sub = firstList.substring(s1 + 5, s2);
						jsonResponseParams[2] = sub;
					}
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 9:
				response = eMQClient.createTransferC2CTransferWallet(args[1], args[2], args[3], args[4], args[5],
						args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15],
						args[16], args[17]);

				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hsC2C1 = (HashMap<String, String>) jsonResponse1.get("info");
				String codeIndiaC2c1 = hsC2C1.get("code");
				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = codeIndiaC2c1;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}

				break;
			case 10:
				response = eMQClient.getListSourcesOfFundsService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 11:
				response = eMQClient.getlistSegmentSourcesOfFundsService(args[1], args[2]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 12:
				response = eMQClient.getlistAllDocumentTypesService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("set")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 13:
				response = eMQClient.getlistSourcesService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 14:
				response = eMQClient.getlistDestinationsService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 15:
				response = eMQClient.getlistCurrenciesService();
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 16:
				response = eMQClient.getlistRelationshipsService();
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 17:

				response = eMQClient.listSegmentRemittancePurposesService(args[1], args[2]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 18:
				response = eMQClient.listAllStatesService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 19:
				response = eMQClient.cancelTransferService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hsCancel = (HashMap<String, String>) jsonResponse1.get("info");
				String codeCancel = hsCancel.get("code");
				if (jsonResponse != null && response.contains("created")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = codeCancel;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 20:
				response = eMQClient.listAllCountriesService();
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 21:
				response = eMQClient.listAllCountriesService();
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}

				break;

			case 22:
				response = eMQClient.listRemittancePurposesService(args[1]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 23:
				response = eMQClient.createTransferJapanB2B(args[1], args[2], args[3], args[4], args[5], args[6],
						args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16],
						args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24], args[25],
						args[26], args[27], args[28], args[29], args[30], args[31], args[32], args[33], args[34],
						args[35], args[36], args[37]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hs = (HashMap<String, String>) jsonResponse1.get("info");
				String code = hs.get("code");
				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = code;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 24:
				response = eMQClient.createTransferIndonesiaB2B(args[1], args[2], args[3], args[4], args[5], args[6],
						args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16],
						args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24], args[25],
						args[26], args[27], args[28], args[29], args[30], args[31], args[32], args[33], args[34],
						args[35], args[36]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					// jsonResponseParams[2] = jsonResponse1.toString();
					jsonResponseParams[2] = " ";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 25:
				response = eMQClient.createTransferIndonesiaC2CBankAccount(args[1], args[2], args[3], args[4], args[5],
						args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15],
						args[16], args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24],
						args[25], args[26], args[27]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					// jsonResponseParams[2] = jsonResponse1.toString();
					jsonResponseParams[2] = " ";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 26:
				response = eMQClient.createTransferIndonesiaC2CCashPickUp(args[1], args[2], args[3], args[4], args[5],
						args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15],
						args[16], args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24],
						args[25], args[26], args[27]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = "";

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 27:
				response = eMQClient.createTransferJapanC2c(args[1], args[2], args[3], args[4], args[5], args[6],
						args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16],
						args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24], args[25],
						args[26], args[27], args[28], args[29], args[30], args[31], args[32], args[33]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hs1 = (HashMap<String, String>) jsonResponse1.get("info");
				String codee = hs1.get("code");
				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = codee;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 28:
				response = eMQClient.listAllActiveCorridorsService();
				jsonResponse1 = eMQClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("code")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 29:
				response = eMQClient.createTransferVietnamC2C(args[1], args[2], args[3], args[4], args[5], args[6],
						args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16],
						args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24], args[25],
						args[26], args[27], args[28]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hss = (HashMap<String, String>) jsonResponse1.get("info");
				String icode = hss.get("code");
				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = icode;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 30:
				response = eMQClient.createTransferPhilippinesC2CCashPickUp(args[1], args[2], args[3], args[4], args[5],
						args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15],
						args[16], args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24],
						args[25], args[26]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> hs4 = (HashMap<String, String>) jsonResponse1.get("info");
				String code4 = hs4.get("code");
				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = code4;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 31:
				response = eMQClient.createTransferPhilippinesC2CBankAccount(args[1], args[2], args[3], args[4],
						args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14],
						args[15], args[16], args[17], args[18], args[19], args[20], args[21], args[22], args[23],
						args[24], args[25], args[26]);
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> philippinesC2CBankAccount = (HashMap<String, String>) jsonResponse1.get("info");
				String codePhilippinesC2CBankAccount = philippinesC2CBankAccount.get("code");
				if (jsonResponse1 != null && response.contains("reference")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = codePhilippinesC2CBankAccount;

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 32:
				response = eMQClient.getBalanceService();
				jsonResponse1 = eMQClient.jsonToMapObject1(response);
				HashMap<String, String> balance = (HashMap<String, String>) jsonResponse1.get("balances");
				String codeBalance = balance.get("Funding USD");
				if (jsonResponse != null && response.contains("Funding USD")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = codeBalance;
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			}
		} catch (

		Exception e) {
			jsonResponseParams[0] = "02";
			jsonResponseParams[1] = "Fail";
			jsonResponseParams[2] = e.getMessage();
			params = Arrays.toString(jsonResponseParams);
			System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
		}
	}

}