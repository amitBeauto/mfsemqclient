package com.mfs.client.emq.client;

/**
 * 
 * @author Anusha
 *
 */
public class Run {
	
	public String get3DigitCountryCode(String twoDigitCountryCode){
		
		String countryCode = "";
		
		if(twoDigitCountryCode.equalsIgnoreCase("IN")){
			countryCode = "IND";
			return countryCode;
		}else if(twoDigitCountryCode.equalsIgnoreCase("CN")){
			countryCode = "CHN";
			return countryCode;
		}else if(twoDigitCountryCode.equalsIgnoreCase("ID")){
			countryCode = "IDN";
			return countryCode;
		}else if(twoDigitCountryCode.equalsIgnoreCase("JP")){
			countryCode = "JPN";
			return countryCode;
		}
		return countryCode;
	}

}